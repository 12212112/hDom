//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file TrackerSD.cc
/// \brief Implementation of the TrackerSD class

#include "TrackerSD.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"
#include "TTree.h"
#include "TFile.h"
#include <vector>
#include "DataManager.hh"
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TrackerSD::TrackerSD(const G4String &name,
                     const G4String &hitsCollectionName)
    : G4VSensitiveDetector(name),
      fHitsCollection(NULL)
{
  collectionName.insert(hitsCollectionName);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TrackerSD::~TrackerSD()
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TrackerSD::Initialize(G4HCofThisEvent *hce)
{
  // Create hits collection

  fHitsCollection = new TrackerHitsCollection(SensitiveDetectorName, collectionName[0]);

  // Add this collection in hce
  G4int hcID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
  hce->AddHitsCollection(hcID, fHitsCollection);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool TrackerSD::ProcessHits(G4Step *aStep,
                              G4TouchableHistory *)
{

  G4Track *pTrack;
  pTrack = aStep->GetTrack();
  G4StepPoint *preStepPoint = aStep->GetPreStepPoint();
  G4TouchableHandle theTouchable = preStepPoint->GetTouchableHandle();
  G4int copyNo = theTouchable->GetReplicaNumber();
  std::vector<float> *DomIddata = DataManager::Instance()->GetDomIddata();
  if (pTrack->GetTouchable()->GetVolume()->GetName() == "PMT")
  {
    DataManager::Instance()->Getpmtvector().at("PmtId").push_back(copyNo);
    DataManager::Instance()->Getpmtvector().at("t0").push_back(aStep->GetPreStepPoint()->GetGlobalTime());
    DataManager::Instance()->Getpmtvector().at("e0").push_back(aStep->GetPreStepPoint()->GetKineticEnergy());
    DataManager::Instance()->Getpmtvector().at("DomId").push_back((*DomIddata)[pTrack->GetTrackID()-1]);
   
    //G4cout<<"Dom id :"<<(*DomIddata)[pTrack->GetTrackID()-1]<<G4endl;
  }
  if (pTrack->GetTouchable()->GetVolume()->GetName() == "SiPM")
  {
    DataManager::Instance()->Getsipmvector().at("SipmId").push_back(copyNo);
    DataManager::Instance()->Getsipmvector().at("t0").push_back(aStep->GetPreStepPoint()->GetGlobalTime());
    DataManager::Instance()->Getsipmvector().at("e0").push_back(aStep->GetPreStepPoint()->GetKineticEnergy());
    DataManager::Instance()->Getsipmvector().at("DomId").push_back((*DomIddata)[pTrack->GetTrackID()-1]);
  }
  return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TrackerSD::EndOfEvent(G4HCofThisEvent *)
{
  //  DataManager::Instance()->BranchData();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
