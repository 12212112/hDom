#include "DataManager.hh"
#include "TTree.h"
#include "TFile.h"
#include "vector"

DataManager::DataManager()
{
}

void DataManager::OpenFile()
{
  InFile = TFile::Open("../Indata/muon_data_vol1_uncomplete.root", "READ");
  //sipmTree = new TTree("hDomSipmHit", "hDomSipmHit");
  //pmtTree = new TTree("hDomPmtHit", "hDomPmtHit");
  hDomTree = InFile->Get<TTree>("DomHit");
  numOfevents = hDomTree->GetEntries();
  hDomTree->SetBranchAddress("t0", &tdatas, &bdatat);
  hDomTree->SetBranchAddress("px", &pxdatas, &bdatapx);
  hDomTree->SetBranchAddress("py", &pydatas, &bdatapy);
  hDomTree->SetBranchAddress("pz", &pzdatas, &bdatapz);
  hDomTree->SetBranchAddress("nx", &nxdatas, &bdatanx);
  hDomTree->SetBranchAddress("ny", &nydatas, &bdatany);
  hDomTree->SetBranchAddress("nz", &nzdatas, &bdatanz);
  hDomTree->SetBranchAddress("e0", &edatas, &bdatae);
  hDomTree->SetBranchAddress("DomId", &DomIddatas, &bdataDomId);
}
void DataManager::WriteFile()
{
  OutFile = TFile::Open("../Outdata/PmtAndSipm.root", "recreate");
  sipmTree = new TTree("hDomSipmHit", "hDomSipmHit");
  pmtTree = new TTree("hDomPmtHit", "hDomPmtHit");
}

void DataManager::EntryData()
{
  bdatat->GetEntry(count);
  bdatapx->GetEntry(count);
  bdatapy->GetEntry(count);
  bdatapz->GetEntry(count);
  bdatanx->GetEntry(count);
  bdatany->GetEntry(count);
  bdatanz->GetEntry(count);
  bdatae->GetEntry(count);
  bdataDomId->GetEntry(count);
}
void DataManager::FillTree()
{
  pmtTree->GetTree()->Fill();
  sipmTree->GetTree()->Fill();
}
void DataManager::Write()
{
  pmtTree->Print();
  sipmTree->Write("", TObject::kOverwrite);
  pmtTree->Write("", TObject::kOverwrite);
  // sipmTree->Write();
  // pmtTree->Write();
  sipmTree->Print();
}
void DataManager::CloseFile()
{
  delete hDomTree;
  delete pmtTree;
  delete sipmTree;
  InFile->Close();
  OutFile->Close();
}
void DataManager::initialize()
{
  pmtdata.insert({"PmtId", std::vector<float>()});
  pmtdata.insert({"t0", std::vector<float>()});
  pmtdata.insert({"e0", std::vector<float>()});
  pmtdata.insert({"DomId", std::vector<float>()});

  sipmdata.insert({"SipmId", std::vector<float>()});
  sipmdata.insert({"t0", std::vector<float>()});
  sipmdata.insert({"e0", std::vector<float>()});
  sipmdata.insert({"DomId", std::vector<float>()});
}
void DataManager::Clear()
{
  pmtdata.at("PmtId").clear();
  pmtdata.at("t0").clear();
  pmtdata.at("e0").clear();
  pmtdata.at("DomId").clear();
  sipmdata.at("SipmId").clear();
  sipmdata.at("t0").clear();
  sipmdata.at("e0").clear();
  sipmdata.at("DomId").clear();
}
void DataManager::BranchData()
{
  sipmTree->Branch("SipmId", &sipmdata.at("SipmId"));
  sipmTree->Branch("t0", &sipmdata.at("t0"));
  sipmTree->Branch("e0", &sipmdata.at("e0"));
  sipmTree->Branch("DomId", &sipmdata.at("DomId"));

  pmtTree->Branch("PmtId", &pmtdata.at("PmtId"));
  pmtTree->Branch("t0", &pmtdata.at("t0"));
  pmtTree->Branch("e0", &pmtdata.at("e0"));
  pmtTree->Branch("DomId", &pmtdata.at("DomId"));
}