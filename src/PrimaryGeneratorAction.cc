//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file PrimaryGeneratorAction.cc
/// \brief Implementation of the PrimaryGeneratorAction class

#include "PrimaryGeneratorAction.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Orb.hh"
#include "G4Event.hh"
#include "G4RunManager.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "TTree.h"
//#include "TString.h"
#include "TFile.h"
#include <array>
#include "string"
#include "EventAction.hh"
#include "DataManager.hh"
#include "G4Sphere.hh"
//#include "TClass.h"
//#include "TDictionary.h"
//#include "TNamed.h"
#include "Randomize.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::PrimaryGeneratorAction()
    : G4VUserPrimaryGeneratorAction()
{
  G4int nofParticles = 1;
  fParticleGun = new G4ParticleGun(nofParticles);

  // default particle kinematic

  G4ParticleDefinition *particleDefinition = G4ParticleTable::GetParticleTable()->FindParticle("opticalphoton");
  fParticleGun->SetParticleDefinition(particleDefinition);
  fParticleGun->SetParticlePolarization(G4ThreeVector(0, 0, 1));
  //the Polarization is set randomly
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fParticleGun;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PrimaryGeneratorAction::GeneratePrimaries(G4Event *anEvent)
{
  DataManager::Instance()->EntryData();
  G4LogicalVolume *hDomLV = G4LogicalVolumeStore::GetInstance()->GetVolume("DOM");
  G4Sphere *hDomBall = NULL;
  hDomBall = dynamic_cast<G4Sphere *>(hDomLV->GetSolid());
  // G4double hDomradius = hDomBall->GetRadius();
  G4double hDomradius = hDomBall->GetRmax();
  G4double save_radius = hDomradius - 1*mm;
  std::vector<float> *tdata =DataManager::Instance()->Gettdata() ;
  std::vector<float> *pxdata = DataManager::Instance()->Getpxdata();
  std::vector<float> *pydata = DataManager::Instance()->Getpydata();
  std::vector<float> *pzdata = DataManager::Instance()->Getpzdata();
  std::vector<float> *nxdata = DataManager::Instance()->Getnxdata();
  std::vector<float> *nydata = DataManager::Instance()->Getnydata();
  std::vector<float> *nzdata = DataManager::Instance()->Getnzdata();
  std::vector<float> *edata = DataManager::Instance()->Getedata();

  for (size_t i = 0; i < nydata->size(); i++)
  {
    fParticleGun->SetParticlePosition(G4ThreeVector((*pxdata)[i] * save_radius, (*pydata)[i] * save_radius, (*pzdata)[i] * save_radius));
    fParticleGun->SetParticleMomentumDirection(G4ThreeVector((*nxdata)[i], (*nydata)[i], (*nzdata)[i]));
    fParticleGun->SetParticleEnergy((*edata)[i] * eV);
    fParticleGun->SetParticleTime((*tdata)[i] * ns);
    fParticleGun->GeneratePrimaryVertex(anEvent);
  }
}