/// \file DataManager.hh
/// \brief Definition of the DataManager class

#ifndef DataManager_h
#define DataManager_h 1

#include "Singleton.hh"
#include "TFile.h"
#include "TTree.h"
#include "vector"

class DataManager : public Singleton<DataManager>
{
public:
    // Open root file at main function
    void OpenFile();
    void WriteFile();
    // Get the number of events 
    int GetNumOfEvent() const { return numOfevents; };

    TTree *GethDomTree() const { return hDomTree; };
    //Entry the data in primary generator
    void EntryData();
    //Count the evet number
    void Counter(){count += 1;};
    //Vector used read
    std::vector<float> *Gettdata() const { return tdatas; };
    std::vector<float> *Getpxdata() const { return pxdatas; };
    std::vector<float> *Getpydata() const { return pydatas; };
    std::vector<float> *Getpzdata() const { return pzdatas; };
    std::vector<float> *Getnxdata() const { return nxdatas; };
    std::vector<float> *Getnydata() const { return nydatas; };
    std::vector<float> *Getnzdata() const { return nzdatas; };
    std::vector<float> *Getedata() const { return edatas; };
    std::vector<float> *GetDomIddata() const { return DomIddatas; };

    TTree *GetPmtTree() { return pmtTree; };
    TTree *GetSipmtTree() { return sipmTree; };
    
    // Fill the trees at the end of G4Event
    void FillTree();
    // Write the file and close at the end of G4run
    void Write();
    void CloseFile();
    //
    TTree *pmtTree;
    TTree *sipmTree;
    TTree *hDomTree;
    int numOfevents;
    int count = 0;
    std::vector<float> *tdatas = 0;
    std::vector<float> *pxdatas = 0;
    std::vector<float> *pydatas = 0;
    std::vector<float> *pzdatas = 0;
    std::vector<float> *nxdatas = 0;
    std::vector<float> *nydatas = 0;
    std::vector<float> *nzdatas = 0;
    std::vector<float> *edatas = 0;
    std::vector<float> *DomIddatas = 0;
    TBranch *bdatat = 0;
    TBranch *bdatapx = 0;
    TBranch *bdatapy = 0;
    TBranch *bdatapz = 0;
    TBranch *bdatanx = 0;
    TBranch *bdatany = 0;
    TBranch *bdatanz = 0;
    TBranch *bdatae = 0;
    TBranch *bdataDomId = 0;
    //intialize the container 
    void initialize();
    void Clear();
    //Clear the data of container 
    void BranchData();
    //container of recepted by PMT and Sipm
    std::map<std::string, std::vector<float>>& Getpmtvector(){return pmtdata;};
    std::map<std::string, std::vector<float>>& Getsipmvector(){return sipmdata;};
    std::map<std::string, std::vector<float>> pmtdata;
    std::map<std::string, std::vector<float>> sipmdata;
private:
    friend class Singleton<DataManager>;
    DataManager();
    TFile *InFile;
    TFile *OutFile;
};

#endif