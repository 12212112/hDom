//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file Det_construct.hh
/// \brief Definition of the Det_construct class

#ifndef Det_construct_h
#define Det_construct_h 1

#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
#include "tls.hh"
#include "G4Sphere.hh"
#include "G4Box.hh"
#include "yaml-cpp/yaml.h"
#include <vector>
class G4VPhysicalVolume;
class G4LogicalVolume;
class G4Material;
class G4UserLimits;
class G4GlobalMagFieldMessenger;

//class Det_messenger;

/// Detector construction class to define materials, geometry
/// and global uniform magnetic field.

class Det_construct : public G4VUserDetectorConstruction
{
public:
  Det_construct();
  virtual ~Det_construct();

public:
  virtual G4VPhysicalVolume *Construct();
  virtual void ConstructSDandField();

  // Set methods
  void SetMaxStep(G4double);
  void SetCheckOverlaps(G4bool);
  YAML::Node rootNode;

private:
  // methods
  void DefineMaterials();
  void SetPSVolumes();
  G4VPhysicalVolume *DefineVolumes();
  // define the solid and logic vloume of 
  G4Sphere *solidPmt;
  G4LogicalVolume *logicPmt;
  G4Box *solidSipm;
  G4LogicalVolume *logicSipm;
  // data members
  G4LogicalVolume *fLogichDom; // pointer to the hDom
  //Material 
  G4Material     *seawater_Material; 
  G4Material*        Gel_Material;     
  G4Material*        Glass_Material;
  G4Material*        plastic_Material;
  G4Material*        PMTandSiPM_Material;            
  G4UserLimits *fStepLimit;  // pointer to user step limits
  G4bool fCheckOverlaps;     // option to activate checking of volumes overlaps
  std::vector<int> theta_array0;  //geometry parameters of theta and phi
  G4int numOftheta0;
  std::vector<std::vector<int>> phi_array0;
  std::vector<int> theta_array1;
  G4int numOftheta1;
  std::vector<std::vector<int>> phi_array1;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
